# Pianoles - Structuur

## Basics
- Waar zit welke noot op een piano?																	*
- Vingerzettingen																					*
- Pedaal gebruik																					*

## Spelen van bladmuziek
- Bladmuziek (F sleutel)																			*
- Centrale C																						*
- ... (Het meeste zul je hier al wel van weten)

## Intervallen
- Benoemen van intervallen volgens de officieele benaming											X
- Benoemen van intervallen in hele en halve tonen													X

## Akkoorden
- Basis akkoorden
  - Majeur(-3)																						*
  - Mineur(-3)																						*
- Omkeringen van Akkoorden																			*
- Sus2 en Sus4 Akkoorden																			*
- Verminderde en Overmatige akkoorden																X
- 7 Akkoorden (7, majeur7, mineur7)																	X
- Powerchords (officieel geen akkoord) I -> V -> VIII												*
- Herkennen van akkoorden (met omkeringen)															X	

## Toonladders
- Het maken van een toonladder																		*
- Kwintencirkel																						X
- Verschillende akkoorden(-3) in een Toonladder														X
- Verschillende akkoorden(-7) in een Toonladder														X
- Blues toonladders																					X
- Spelen van toonladders met linker en rechterhand													*
- Herkennen van een toonladder.																		X
- Semantiek: Dominante, Subdominante akkoorden														X

## Spelen op oor
- Ontdekken van Toonladders door akkoorden
- Ontdekken van Toonladders door melodie

## Arrangeren
- Spelen van akkoorden met de melodielijn.
- Toevoegde 9de noot.

## Improviseren
- Akkoorden schema's.

## Componeren
- De 4 akkoorden waar iedereen het over heeft: (I, IV, V, VI)
- Wisselen van toonladders

## Verschillende Stijlen
- Jazz
- Ragtime (hops)
- Klassiek
- Blues

## Trukendoos Uitbereiden
- Ghost notes
- V-trap van majeur naar mineur
- VI -> VIb -> VI
- Dynamiek (Snel vs Sloom, Hardheid)
- Ritmische patronen


