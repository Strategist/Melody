# List of compositions
Below is a list of compositions and their current state.

-----------------------------------
Finished Compositions
-----------------------------------
## 1. Life
State: Finished Composition
Midi/Sheetmusic: Yes/Yes
Dedicated to: My Childhood			(Signature Song)

## 2. One day we'll meet again / Once again / Once more / One day
State: Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: My Father, Charlotte	(Funeral Theme)

## 3. True Love
State: Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: Charlotte and Joran 	(Wedding theme)

## 4. A cup of tea with a friend.
State: (Near Finished) Composition
Midi/Sheetmusic: No/No
Dedicated to: ???					(Koen, JP, Yoram, Esdra, Job, Oliver)

## 5. Remember
State: Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: ??? 					(Reunion - Melanchthon Bergschenhoek)

## 6. Under the Cherry Tree / Journey to the Light
State: Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: Neigbours				(Neighbors)

## 7. Hope
State: Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: Meike, Femke, Tessa, Guido, Henri�tte van der Kooij

## 8. Rising Stars (Second)
State: (Near-)Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: Truus van Bragt
See: https://www.youtube.com/watch?v=Q1XYvQ6NdNc

## 9. Funky (A friend like me) - song.
State: (Near-)Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: ???

## 10. Oceans
State: (Near-)Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: Femke	& Meike			()

## 11. Fairy Tale
State: (Near-)Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: ???

## 12. Hero's Childhood
State: (Near-)Finished Composition
Midi/Sheetmusic: No/No
Dedicated to: ???
Link: https://www.youtube.com/watch?v=wnrqLwEfn-E

## 13. Faith, Hope and Love
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: Meike van der Kooij

## 14. Clouds
State: Finished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 15. Stars will never fade
State: Finished Composition
Midi: None
Sheetmusic: None
Dedicated to: Aura

-----------------------------------
Partial Compositions
-----------------------------------

## 1. Noir
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: Noir from Libanon

## 2. Laura's song?
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: Laura?

## 5. Inside my head
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 7. Chord number 4b
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 8. Jungle?
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 9. Fast song in A, double fingered
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 11. River flows kinda-like
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## 12. Meike song, G, GG, F, F#, F, G, D
State: Unfinished Composition
Midi: None
Sheetmusic: None
Dedicated to: ???

## https://www.youtube.com/watch?v=Ff5VOEV96Z4

Look at youtube / inside keyboard / look for whatsapp media for more.